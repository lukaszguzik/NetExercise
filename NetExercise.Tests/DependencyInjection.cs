﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace NetExercise.Tests
{
    public static class DependencyInjection
    {
        public static IServiceProvider serviceProvider;

        static DependencyInjection()
        {
            var serviceCollection = new ServiceCollection();

            BusinessLogic.DependencyInjection.Configure(serviceCollection);

            serviceProvider = serviceCollection.BuildServiceProvider();
        }
    }
}
