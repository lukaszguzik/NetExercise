﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using NetExercise.Common.Models;
using NUnit.Framework;

namespace NetExercise.Tests
{
    [TestFixture]
    class ConverterOperationsTests
    {
        [Test]
        public void ConvertXmlSimple()
        {
            var converter = DependencyInjection.serviceProvider.GetRequiredService<Common.Operations.IConverterOperations>();
            var input = new ConvertModel
            {
                Type = ConvertModel.ConvertType.Xml,
                InputText = @"Mary had a little lamb. Peter called for the wolf, and Aesop came. 
Cinderella likes shoes."
            };

            var output = converter.ConvertText(input);

            //remove BOM character from output before result comparation
            if(output?.OutputText.Length > 0
                && Encoding.UTF8.GetBytes(new char[] { output.OutputText[0] }).SequenceEqual(Encoding.UTF8.GetPreamble()))
            {
                output.OutputText = output.OutputText.Substring(1);
            }

            Assert.AreEqual(Resource.XmlOutput, output.OutputText);
        }

        [Test]
        public void ConvertXmlWhitespaceOk()
        {
            var converter = DependencyInjection.serviceProvider.GetRequiredService<Common.Operations.IConverterOperations>();
            var input = new ConvertModel
            {
                Type = ConvertModel.ConvertType.Xml,
                InputText = @"  Mary   had a little  lamb  . 


  Peter   called for the wolf   ,  and Aesop came .
 Cinderella  likes shoes."
            };

            var output = converter.ConvertText(input);

            //remove BOM character from output before result comparation
            if (output?.OutputText.Length > 0
                && Encoding.UTF8.GetBytes(new char[] { output.OutputText[0] }).SequenceEqual(Encoding.UTF8.GetPreamble()))
            {
                output.OutputText = output.OutputText.Substring(1);
            }

            Assert.AreEqual(Resource.XmlOutput, output.OutputText);
        }

        [Test]
        public void ConvertXmlEmptyString()
        {
            var converter = DependencyInjection.serviceProvider.GetRequiredService<Common.Operations.IConverterOperations>();
            var input = new ConvertModel
            {
                Type = ConvertModel.ConvertType.Xml,
                InputText = string.Empty
            };

            var output = converter.ConvertText(input);

            //remove BOM character from output before result comparation
            if (output?.OutputText.Length > 0
                && Encoding.UTF8.GetBytes(new char[] { output.OutputText[0] }).SequenceEqual(Encoding.UTF8.GetPreamble()))
            {
                output.OutputText = output.OutputText.Substring(1);
            }

            Assert.AreEqual(Resource.XmlEmpty, output.OutputText);
        }

        [Test]
        public void ConvertCsvSimple()
        {
            var converter = DependencyInjection.serviceProvider.GetRequiredService<Common.Operations.IConverterOperations>();
            var input = new ConvertModel
            {
                Type = ConvertModel.ConvertType.Csv,
                InputText = @"Mary had a little lamb. Peter called for the wolf, and Aesop came. 
Cinderella likes shoes."
            };

            var output = converter.ConvertText(input);

            Assert.AreEqual(Resource.CsvOutput, output.OutputText);
        }

        [Test]
        public void ConvertCsvWhitespaceOk()
        {
            var converter = DependencyInjection.serviceProvider.GetRequiredService<Common.Operations.IConverterOperations>();
            var input = new ConvertModel
            {
                Type = ConvertModel.ConvertType.Csv,
                InputText = @"  Mary   had a little  lamb  . 


  Peter   called for the wolf   ,  and Aesop came .
 Cinderella  likes shoes."
            };

            var output = converter.ConvertText(input);

            Assert.AreEqual(Resource.CsvOutput, output.OutputText);
        }

        [Test]
        public void ConvertCsvEmptyString()
        {
            var converter = DependencyInjection.serviceProvider.GetRequiredService<Common.Operations.IConverterOperations>();
            var input = new ConvertModel
            {
                Type = ConvertModel.ConvertType.Csv,
                InputText = string.Empty
            };

            var output = converter.ConvertText(input);

            Assert.AreEqual(string.Empty, output.OutputText);
        }
    }
}
