import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConvertModel } from '../models/convert.model';
import { environment } from '../../environments/environment';
import { ConvertResponseModel } from '../models/convertResponse.model';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConverterService {

  constructor(private httpClient: HttpClient) { }

  convertText(inputData: ConvertModel) : Observable<ConvertResponseModel> {
    return this.httpClient.post<ConvertResponseModel>(`${environment.apiUrl}/converter`, inputData)
      .pipe(
        catchError(this.handleError('convertText', new ConvertResponseModel()))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(`${operation} error`);
      console.error(error);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
