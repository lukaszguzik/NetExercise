export enum ConvertType {
  Xml = 1,
  Csv = 2
}

export class ConvertModel {
  type: ConvertType;
  inputText: string;
}
