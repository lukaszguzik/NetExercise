import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConvertModel, ConvertType } from '../../models/convert.model';
import { ConvertResponseModel } from '../../models/convertResponse.model';
import { ConverterService } from '../../services/converter.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.css']
})
export class ConverterComponent implements OnInit {

  convertModel = new ConvertModel;
  convertResponseModel = new ConvertResponseModel;
  convertTypes = [
    { name: 'XML format', value: ConvertType.Xml },
    { name: 'CSV format', value: ConvertType.Csv }
  ];

  constructor(private convertService: ConverterService) { }

  ngOnInit() {
    this.convertModel.inputText = '';
    this.convertModel.type = ConvertType.Xml;
    this.convertResponseModel.outputText = '';
  }

  convert() {
    console.log(this.convertModel.inputText);

    this.convertService.convertText(this.convertModel)
      .subscribe(
        result => {
          this.convertResponseModel = result;
          console.log(result);
        }
      );
  }
}
