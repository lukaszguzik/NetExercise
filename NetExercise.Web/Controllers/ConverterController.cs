using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetExercise.Common.Models;
using NetExercise.Common.Operations;

namespace NetExercise.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Converter")]
    public class ConverterController : Controller
    {
        private IConverterOperations converterOperations;

        public ConverterController(IConverterOperations converterOperations)
        {
            this.converterOperations = converterOperations;
        }

        /// <summary>
        /// Action converting text to specified format
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public virtual IActionResult ConvertText([FromBody] ConvertModel input)
        {
            if (ModelState.IsValid)
            {
                var result = converterOperations.ConvertText(input);
                return Ok(result);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
