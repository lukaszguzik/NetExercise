# NetExercise.Web

Project containing ASP.NET Core REST API with Angular 6 Web application

# NetExercise.Common

Project containing common code:
* Data models passed between Angular and ASP.NET Core REST API
* Interfaces defining application business logic

# NetExercise.BusinessLogic

Project containing application business logic:
* Parsing input data with usage of [Sprache](https://github.com/sprache/Sprache) - library for creating parsers in code
* Writing parsed data to XML or CSV format

# NetExercise.Tests

Project with NUnit unit tests