﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using NetExercise.BusinessLogic.Converter;
using NetExercise.BusinessLogic.Converter.Formatters;
using NetExercise.Common.Operations;

namespace NetExercise.BusinessLogic
{
    public static class DependencyInjection
    {
        public static void Configure(IServiceCollection services)
        {
            services.AddScoped<IConverterOperations, ConverterOperations>();
            services.AddScoped<ITextFormatterFactory, TextFormatterFactory>();
        }
    }
}
