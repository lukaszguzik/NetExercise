﻿using System;
using System.Collections.Generic;
using System.Text;
using NetExercise.BusinessLogic.Converter.Formatters;
using NetExercise.Common.Models;
using NetExercise.Common.Operations;

namespace NetExercise.BusinessLogic.Converter
{
    class ConverterOperations : IConverterOperations
    {
        private readonly ITextFormatterFactory textFormatterFactory;

        public ConverterOperations(ITextFormatterFactory textFormatterFactory)
        {
            this.textFormatterFactory = textFormatterFactory;
        }

        public ConvertResponseModel ConvertText(ConvertModel input)
        {
            Text textModel;
            if (!string.IsNullOrEmpty(input?.InputText))
            {
                textModel = TextParser.ParseText(input.InputText);
            }
            else
            {
                textModel = new Text
                {
                    Sentences = new List<Sentence>()
                };
            }

            var formatter = textFormatterFactory.CreateInstance(input.Type);

            return new ConvertResponseModel
            {
                OutputText = formatter.FormatText(textModel)
            };
        }
    }
}
