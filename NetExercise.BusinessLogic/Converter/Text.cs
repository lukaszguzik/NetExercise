﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace NetExercise.BusinessLogic.Converter
{
    public class Text
    {
        public IEnumerable<Sentence> Sentences { get; set; }
    }
}
