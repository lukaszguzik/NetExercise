﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NetExercise.BusinessLogic.Converter
{
    public class Sentence
    {
        public IEnumerable<string> Words { get; set; }
    }
}
