﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sprache;

namespace NetExercise.BusinessLogic.Converter
{
    static class TextParser
    {
        private static readonly Parser<IEnumerable<char>> sentenceDelimiter = Parse.Chars(".!?").AtLeastOnce().Token();

        private static readonly Parser<IEnumerable<char>> wordDelimiter = Parse.AnyChar
            .Except(Parse.LetterOrDigit)
            .Except(sentenceDelimiter)
            .AtLeastOnce()
            .Token();

        private static readonly Parser<string> wordParser = Parse.LetterOrDigit.AtLeastOnce().Text().Token();

        private static readonly Parser<Sentence> sentenceParser =
            from words in wordParser.DelimitedBy(wordDelimiter.Many())
            select new Sentence { Words = words.OrderBy(e => e) };

        private static readonly Parser<Text> textParser =
            from leading in Parse.AnyChar.Except(Parse.LetterOrDigit).Many()
            from sentences in Parse.Optional(sentenceParser.DelimitedBy(sentenceDelimiter.Many()))
            select new Text { Sentences = sentences.GetOrDefault() };

        public static Text ParseText(string input)
        {
            return textParser.Parse(input);
        }
    }
}
