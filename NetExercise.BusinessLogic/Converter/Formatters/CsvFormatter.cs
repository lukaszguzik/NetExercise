﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetExercise.BusinessLogic.Converter.Formatters
{
    class CsvFormatter : ITextFormatter
    {
        private const string WORD_HEADER_NAME = "Word";
        private const string SENTENCE_ROW_BEGINING = "Sentence";

        public string FormatText(Text input)
        {

            var output = new StringBuilder();

            if(input.Sentences != null && input.Sentences.Count() > 0)
            {
                var maxWords = GetLongestSentenceWordsCount(input);
                if(maxWords > 0)
                {
                    for (int i = 1; i <= maxWords; i++)
                    {
                        output.Append(", ").Append(WORD_HEADER_NAME).Append(' ').Append(i);
                    }
                    output.AppendLine();

                    int sentenceIdx = 1;
                    foreach (var sentence in input.Sentences)
                    {
                        if(sentenceIdx > 1)
                        {
                            output.AppendLine();
                        }

                        WriteWordsRecord(output, sentenceIdx++, sentence.Words);
                    }
                }
            }

            return output.ToString();
        }

        private int GetLongestSentenceWordsCount(Text input)
        {
            return input.Sentences.Max(e => e.Words?.Count() ?? 0);
        }

        private void WriteWordsRecord(StringBuilder sb, int sentenceIndex, IEnumerable<string> words)
        {
            sb.Append(SENTENCE_ROW_BEGINING).Append(' ').Append(sentenceIndex);
            if(words != null)
            {
                foreach (var word in words)
                {
                    sb.Append(", ").Append(word);
                }
            }
        }
    }
}
