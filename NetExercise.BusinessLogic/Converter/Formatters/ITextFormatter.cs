﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetExercise.BusinessLogic.Converter.Formatters
{
    interface ITextFormatter
    {
        string FormatText(Text input);
    }
}
