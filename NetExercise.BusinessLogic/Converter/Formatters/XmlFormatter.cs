﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace NetExercise.BusinessLogic.Converter.Formatters
{
    class XmlFormatter : ITextFormatter
    {
        private const string TEXT_ELEMENT_NAME = "text";
        private const string SENTENCE_ELEMENT_NAME = "sentence";
        private const string WORD_ELEMENT_NAME = "word";

        public string FormatText(Text input)
        {
            var xmlWriterSettings = new XmlWriterSettings
            {
                Encoding = Encoding.UTF8,
                Indent = true,
                IndentChars = new string(' ', 4)
            };

            var sb = new StringBuilder();
            using (var xmlWriter = XmlWriter.Create(sb, xmlWriterSettings))
            {
                xmlWriter.WriteStartDocument(true);
                xmlWriter.WriteStartElement(TEXT_ELEMENT_NAME);

                if (input.Sentences != null)
                {
                    foreach (var sentence in input.Sentences)
                    {
                        xmlWriter.WriteStartElement(SENTENCE_ELEMENT_NAME);
                        WriteAllWords(xmlWriter, sentence.Words);
                        xmlWriter.WriteEndElement();
                    }
                }

                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
                xmlWriter.Flush();

                var output = sb.ToString();
                return output;
            }


        }

        private void WriteAllWords(XmlWriter xmlWriter, IEnumerable<string> words)
        {
            if(words != null)
            {
                foreach (var word in words)
                {
                    xmlWriter.WriteElementString(WORD_ELEMENT_NAME, word);
                }
            }
        }
    }
}
