﻿using System;
using System.Collections.Generic;
using System.Text;
using NetExercise.Common.Models;

namespace NetExercise.BusinessLogic.Converter.Formatters
{
    class TextFormatterFactory : ITextFormatterFactory
    {
        public ITextFormatter CreateInstance(ConvertModel.ConvertType convertType)
        {
            switch (convertType)
            {
                case ConvertModel.ConvertType.Xml:
                    return new XmlFormatter();

                case ConvertModel.ConvertType.Csv:
                    return new CsvFormatter();

                default:
                    throw new ArgumentException($"Unknown convert type {convertType}");
            }
        }
    }
}
