﻿using System;
using System.Collections.Generic;
using System.Text;
using NetExercise.Common.Models;

namespace NetExercise.BusinessLogic.Converter.Formatters
{
    interface ITextFormatterFactory
    {
        ITextFormatter CreateInstance(ConvertModel.ConvertType convertType);
    }
}
