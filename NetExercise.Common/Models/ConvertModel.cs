﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NetExercise.Common.Models
{
    /// <summary>
    /// Model containing data to convert
    /// </summary>
    public class ConvertModel
    {
        /// <summary>
        /// Requested convert output format
        /// </summary>
        public enum ConvertType
        {
            /// <summary>
            /// Text should be converted to XML
            /// </summary>
            Xml = 1,

            /// <summary>
            /// Text should be converted do CSV
            /// </summary>
            Csv = 2
        }

        /// <summary>
        /// Conversion type
        /// </summary>
        [Required]
        [EnumDataType(typeof(ConvertType))]
        public ConvertType Type { get; set; }

        /// <summary>
        /// Text to convert
        /// </summary>
        [Required]
        public string InputText { get; set; }
    }
}
