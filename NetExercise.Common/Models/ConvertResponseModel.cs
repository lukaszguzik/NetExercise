﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetExercise.Common.Models
{
    /// <summary>
    /// Model containing converted data
    /// </summary>
    public class ConvertResponseModel
    {
        /// <summary>
        /// Converted text
        /// </summary>
        public string OutputText { get; set; }
    }
}
