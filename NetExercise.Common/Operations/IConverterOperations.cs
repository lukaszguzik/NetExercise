﻿using System;
using System.Collections.Generic;
using System.Text;
using NetExercise.Common.Models;

namespace NetExercise.Common.Operations
{
    /// <summary>
    /// Interface defining converter business operations
    /// </summary>
    public interface IConverterOperations
    {
        /// <summary>
        /// Converts text
        /// </summary>
        /// <param name="input">Convert text data with requested format</param>
        /// <returns>Text converted to specified format</returns>
        ConvertResponseModel ConvertText(ConvertModel input);
    }
}
